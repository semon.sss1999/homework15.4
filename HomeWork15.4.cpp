#include <iostream>

class Animal
{
public:
	virtual void Voice() 
	{
		//std::cout << " Animal voice "// << std::endl; 
	}
};


class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Wood " << std::endl;

	}
};


class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow " << std::endl;

	}
};


class Mouse : public Animal
{
public:
	void Voice() override
	{
		std::cout << "pi, pi, pi" << std::endl;

	}
};


int main()
{
	static int CountAnimal = 3;

	Cat* C = new Cat();
	Dog* D = new Dog();
	Mouse* M = new Mouse();
	

	Animal** Array = new Animal * [CountAnimal]{C,D,M};

	for (int i = 0; i < CountAnimal; i++)
	{
		Array[i]->Voice();
	}
	return 0;
}




